/* Scroll Navbar */

$(window).scroll(function () {

    var scrollanimated = $(window).scrollTop();

    if (scrollanimated > 100) {
        $(".navbar.navbar-fixed-top").addClass("scroll-fixed-navbar");
    } else if (scrollanimated < 100) {
        $(".navbar.navbar-fixed-top").removeClass("scroll-fixed-navbar");
    }
});

/* Superslider */

$(document).ready(function () {
    $('#slides').superslides({
        animation: 'fade',
        pagination: true,
    });
});

/* Stellar Paralax Effect */

$(document).ready(function () {
    $(window).stellar();
});

/* Progress Bars wit Pie Chart */

$(window).scroll(function () {

    "use strict";

    if ($().easyPieChart) {
        var count = 0;
        var colors = ['#fa6900'];
        $('.percentage').each(function () {

            var imagePos = $(this).offset().top;
            var topOfWindow = $(window).scrollTop();
            if (imagePos < topOfWindow + 600) {

                $(this).easyPieChart({
                    barColor: colors[count],
                    trackColor: '#202020',
                    scaleColor: false,
                    scaleLength: false,
                    lineCap: 'butt',
                    lineWidth: 8,
                    size: 130,
                    rotate: 0,
                    animate: 2000,
                    onStep: function (from, to, percent) {
                        $(this.el).find('.percent').text(Math.round(percent));
                    }
                });
            }

            count++;
            if (count >= colors.length) {
                count = 0
            };
        });
    }

});

/*----------------------------------------------------*/
/*	Statistic Counter
/*----------------------------------------------------*/

/*$('.statistic-block').each(function () {
    $(this).appear(function () {
        var $endNum = parseInt($(this).find('.statistic-number').text());
        $(this).find('.statistic-number').countTo({
            from: 0,
            to: $endNum,
            speed: 3000,
            refreshInterval: 30,
        });
    }, {
        accX: 0,
        accY: 0
    });
});*/

$(function () {
    var viewport = $(window).width();
    if (viewport > 1024) {
        //$('.section-title').hide();

        $(window).scroll(function () {
            //detect height section intro;
            var hintro = $('.section-intro').height() / 2;
            console.log(hintro);
            if ($(window).scrollTop() > hintro) {
                $('.section-title').animate({
                    opacity: 1
                }, 400);

                $('.features-1-content').css('overflow', 'hidden');

                // animate left col
                if($('.features-1 .features-left').length) {
                    $('.features-left').animate({
                        opacity: 1,
                        left: 0
                    }, 600);
                }

                // animate right col
                if($('.features-1 .features-right').length) {
                    $('.features-right').animate({
                        opacity: 1,
                        right: 0
                    }, 600);
                }

            } else {
                $('.section-title').stop(true).animate({
                    opacity: 0
                }, 400);

                //animate left col
                if($('.features-1 .features-left').length) {
                    $('.features-left').stop(true).animate({
                        opacity: 0,
                        left: -300
                    }, 600);
                }

                //animate right col
                if($('.features-1 .features-right').length) {
                    $('.features-right').stop(true).animate({
                        opacity: 0,
                        right: -300
                    }, 600);
                }
            }
        });
    }
});
